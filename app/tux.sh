#!/bin/bash
# This is TUX

echo
echo  -e "\033[33;5mLOADING\033[0m"


for pc in $(seq 0 10 100); do
    echo -ne "$pc%\033[0K\r"
    sleep 1
done
echo

cat << 'EOF'
         _nnnn_
        dGGGGMMb     .""""""""""""""".
       @p~qp~~qMb    | Hello I'm TUX |
       M @||@) M|   _;...............'
       @,----.JM| -'
      JS \__/  qKL
     dZP        qKRb
    dZP          qKKb
   fZP            SMMb
   HZM            MMMM
   FqM            MMMM
    dM           MMMMb  
 __| ".        |\dS"qML
 |    `.       | `' \Zq
_)      \.___.,|     .'
\____   )MMMMMM|   .'
     `-'       `--'
EOF

echo
echo  -e "\033[33;5mBYE-BYE!\033[0m"

echo 
echo "Authors: pbarczi,fsabol,kolka,matus,ekosteln,pegri,makovac"
